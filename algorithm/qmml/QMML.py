#!/usr/bin/env python

import numpy as np
import os, multiprocessing, subprocess, glob, shutil
import cPickle as pickle

from ase.calculators.calculator import Calculator
from ase.data import atomic_numbers
from ase.io.trajectory import TrajectoryWriter
from ase import io
from amp.model import LossFunction
from amp import Amp
from amp.utilities import TrainingConvergenceError

class QMML(Calculator):
    """ Quantum Mechanics - Machine Learning (QMML) calculator
        based on AMP and a DFT calculator. """
    implemented_properties = ['energy', 'forces']
    def __init__(self, selection, qmcalc, mmcalc, qmpbc=None, qmvac=None,
                 qmcell=None, retrain=None, traintraj=[], restart=False,
                 ncores=None, InterSteps=1):
        '''
        parameters:

        selection: list of int, QM region atoms indices
            Selection out of all the atoms that belong to the QM part.
        qmcalc: Calculator object or string 'gpaw'
            QM-calculator. If it's gpaw, a 'gpaw_driver.py' file needed.
        mmcalc: Calculator object
            MM-calculator. We use AMP here.
        qmpbc: one or three bool, default is None
            None, the same as the atoms object
            Periodic boundary conditions flags of the QM region.
            Examples: True, False, 0, 1, (1, 1, 0), (True, False, False).
        qmvac: float, default is None
            None, the same unit cell size as the whole Atoms object
            If vacuum=10.0 there will thus be 10 Angstrom of vacuum
            on each side of axis=0 (x axis).
        qmcell: 3x3 matrix or length 3 or 6 vector
            default is None, same as the whole Atoms object unit cell
            QM region unit cell
        retrain: dict or None, default is None
            None, the AMP will not be retrained.
            Dict, it passes the convergence criteria to retrain AMP by QM calculations.
            such as {'energy_maxresid': 2.0e-3,
                     'force_maxresid': 5.0e-2}
                     {'energy_rmse': 0.001,
                      'energy_maxresid': None,
                      'force_rmse': 0.005,
                      'force_maxresid': None, }
        traintraj: Usually it's a list of atoms object, images used to train AMP
            Can be any image format that AMP training can accept
        restart: Boolean, apply only when retrain=True
        InterSteps: int > 0, the number of calculator calls between AMP retrains
            apply only when retrain=True
            default is 1, the retrain happens at every call
        ncores: int, the number of total cpus.
            If None, guess the ncores by multiprocessing module for GPAW calc.
        '''
        self.selection  = selection
        self.qmcalc     = qmcalc
        self.mmcalc     = mmcalc
        self.qmatoms    = None
        self.qmpbc      = qmpbc
        self.qmvac      = qmvac
        self.qmcell     = qmcell
        self.qmtraj     = './qmtraj.traj'
        self.retrain    = retrain
        if self.retrain:
            self.traintraj  = traintraj
            self.restart    = restart
            self.callcnt    = 0
            self.InterSteps = InterSteps

        if ncores:
            self.ncores  = ncores
        elif self.qmcalc == 'gpaw':
            self.ncores  = multiprocessing.cpu_count()

        try:
            self.name    = 'QM region {0} - QM region {1} + All {1}'.format(qmcalc.name, mmcalc.name)
        except: # Dacapo & GPAW
            self.name    = 'QM region - QM region {0} + All {0}'.format(mmcalc.name)

        Calculator.__init__(self)

    def initialize_qm(self, atoms):
        constraints       = atoms.constraints
        atoms.constraints = []
        self.qmatoms      = atoms[self.selection]
        atoms.constraints = constraints
        if self.qmpbc:
            self.qmatoms.pbc = self.qmpbc
        if self.qmcell is not None:
            self.qmatoms.set_cell( self.qmcell, scale_atoms=False )
            self.qmatoms.center()
        if self.qmvac:
            self.qmatoms.center( vacuum=self.qmvac, axis=0 )

    def calculate(self, atoms, properties, system_changes):
        Calculator.calculate( self, atoms, properties, system_changes )
        self.initialize_qm(atoms)

        if self.retrain and self.restart:
            self.retrain_amp()
            self.restart = False

        if self.qmcalc == 'gpaw':
            qmenergy, qmforces = self.gpaw_calculate()
        else:
            for item in glob.iglob('*.nc'): #for Dacapo
                self.qmcalc.restart()
            self.qmatoms.set_calculator(self.qmcalc)
            qmenergy = self.qmcalc.get_potential_energy(self.qmatoms)
            qmforces = self.qmcalc.get_forces(self.qmatoms)
            writer   = TrajectoryWriter( self.qmtraj, mode='a', atoms=self.qmatoms )
            writer.write()

        if self.retrain:
            if self.callcnt == 0:
                self.callcnt = self.InterSteps
                self.retrain_amp()

        energy     = self.mmcalc.get_potential_energy(atoms)
        forces     = self.mmcalc.get_forces(atoms)
        energy    += qmenergy
        energy    -= self.mmcalc.get_potential_energy(self.qmatoms)
        forces[self.selection] += qmforces
        forces[self.selection] -= self.mmcalc.get_forces(self.qmatoms)
        self.results['energy']  = energy
        self.results['forces']  = forces
        if self.retrain:
            self.callcnt -= 1

    def retrain_amp(self):
        if self.restart:
            images = self.traintraj
            files  = [ i for i in os.listdir('./amp-checkpoints/')]
            files.sort( key=lambda x: os.path.getmtime(os.path.join('./amp-checkpoints/', x)) )
            shutil.copyfile( os.path.join('./amp-checkpoints/', files[-1]), './restart.amp' )
            calc   = Amp.load( './restart.amp' )
        else:
            images = self.readimages()
            calc   = self.mmcalc
        #use the max force res as the criteria
        calc.model.lossfunction = LossFunction( convergence=self.retrain )
        try:
            calc.train( images=images, overwrite=True )
        except TrainingConvergenceError:
            calc = Amp.load('./amp-untrained-parameters.amp')
            os.rename('./amp-untrained-parameters.amp', '1-untrained-parameters.amp')
        self.mmcalc = calc

    def readimages(self):
        atoms = io.read( self.qmtraj, index=-1 )
        self.traintraj.append( atoms )
        return self.traintraj

    def gpaw_calculate(self, tmpatoms='gpaw_driver.pckl'):
        pickle.dump( (self.qmatoms), open(tmpatoms,'wb') )
        gpaw_driver = './gpaw_driver.py'
        #self.qmtraj is written in the gpaw_driver.py
        subprocess.call('mpiexec -n %d python %s %s %s' % (self.ncores,
                                                           gpaw_driver,
                                                           tmpatoms,
                                                           self.qmtraj),
                        shell=True)
        gpawatoms = io.read( self.qmtraj, index=-1 )
        qmenergy  = gpawatoms.get_potential_energy()
        qmforces  = gpawatoms.get_forces( apply_constraint=False )
        os.remove(tmpatoms)
        return qmenergy, qmforces
